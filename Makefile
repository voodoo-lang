include Makefile.cfg

SUBDIRS = bin doc lib/voodoo test
TARGETS = Makefile.cfg voodoo.gemspec voodoo-$(VERSION).gem

default : lib compiler

all : compiler test rdoc

clean :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) clean); done

compiler : lib
	cd bin && $(MAKE)

distclean :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) distclean); done
	-rm $(TARGETS)

doc :
	cd doc && $(MAKE)

gem : gem-$(VERSION)

install :
	cd lib && $(MAKE) install
	cd bin && $(MAKE) install
	cd doc && $(MAKE) install

lib :
	cd lib/voodoo && $(MAKE)

rdoc :
	cd doc && $(MAKE) rdoc

test : lib compiler
	cd test && $(MAKE) test

Makefile.cfg : configure
	./configure

gem-$(VERSION) : lib compiler voodoo.gemspec
	rm lib/voodoo/config.rb
	$(MAKE) DEFAULT_ARCHITECTURE=auto DEFAULT_FORMAT=elf NASM=nasm GAS=as
	$(GEM) build voodoo.gemspec

voodoo.gemspec : voodoo.gemspec.in Makefile.cfg
	env VERSION='$(VERSION)' $(RUBY) voodoo.gemspec.in > voodoo.gemspec

.PHONY : all compiler clean default distclean doc install lib rdoc test
