require 'voodoo/config'
require 'voodoo/code_generator'
require 'voodoo/compiler'
require 'voodoo/error'
require 'voodoo/parser'

# = Voodoo - Code Generation for Multiple Target Platforms
#
# This module implements a compiler for the {Voodoo programming
# language}[http://inglorion.net/documents/designs/voodoo/], a simple
# programming language designed to be a thin abstraction of the CPU's
# native instruction set.
#
# The compiler consists of four parts:
#
# 1. The parser (Voodoo::Parser), which reads 
#    Voodoo[http://inglorion.net/documents/designs/voodoo/] source code and
#    turns it into Ruby[http://www.ruby-lang.org/] objects.
#
# 2. The validator (Voodoo::Validator), which takes Voodoo code in Ruby
#    object form and checks that it is valid. The parser calls the validator,
#    so if you are using the parser to get Voodoo code, it will already have
#    been validated.
#
# 3. The code generator (a class that implements the methods of
#    Voodoo::CommonCodeGenerator), which provides methods that generate
#    code for the target platform.
#
# 4. The compiler driver (Voodoo::Compiler), which reads from the parser
#    and calls the appropriate methods of the code generator.
#
# The parser, validator, and code generators can be used on their own.
# For example, you could use the code generator to generate native
# code for your own programming language without first creating a
# Voodoo[http://inglorion.net/documents/designs/voodoo/] program.
#
# Instead of instantiating a code generator directly, it is recommended
# that you use Voodoo::CodeGenerator.get_generator to obtain a suitable
# code generator for your target platform.
#
# A few examples to clarify the usage of the module:
#
# The following code compiles the source file <tt>test.voo</tt> to an ELF
# object file called <tt>test.o</tt> containing object code for i386:
#
#   require 'voodoo'
#
#   File.open('test.voo') do |infile|
#     parser = Voodoo::Parser.new infile
#     generator = Voodoo::CodeGenerator.get_generator :architecture => :i386,
#                                                     :format => :elf
#     File.open('test.o', 'w') do |outfile|
#       compiler = Voodoo::Compiler.new parser, generator, outfile
#       compiler.compile
#     end
#   end
#
# The following code uses the code generator API to create an object file
# without the need to create a
# Voodoo[http://inglorion.net/documents/designs/voodoo/] program:
#
#   require 'voodoo'
#
#   generator = Voodoo::CodeGenerator.get_generator
#
#   generator.add :functions, [:export, :fact], [:label, :fact]
#   generator.add_function [:n],
#     [:ifle, [:n, 1],
#       # then
#       [[:return, 1]],
#       # else
#       [[:let, :x, :sub, :n, 1],
#        [:set, :x, :call, :fact, :x],
#        [:return, :mul, :n, :x]]]
#
#   File.open('fact.o', 'w') { |outfile| generator.write outfile }
#

module Voodoo
end
