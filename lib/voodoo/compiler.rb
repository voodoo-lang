require 'voodoo/error'
require 'voodoo/parser'

module Voodoo
  # Voodoo compiler driver.
  # The compiler driver reads input from a parser (see
  # Voodoo::Parser), feeds it to a code generator (see
  # Voodoo::CommonCodeGenerator), and writes the generated code.
  #
  # An example of its usage can be found on the main page for the
  # Voodoo module.
  class Compiler
    class Error < Voodoo::Error
      def initialize errors
        @errors = errors
      end

      attr_reader :errors

      def message
        msg = ""
        @errors.each {|e| msg << e.message << "\n"}
        msg
      end
    end

    # Initializes a compiler.
    #
    # Parameters:
    # [parser] the parser to be used (see Voodoo::Parser)
    # [code_generator] the code generator to be used
    #                  (see Voodoo::CommonCodeGenerator)
    # [output] an IO object. The generated code will be written to it
    def initialize parser, code_generator, output
      @parser = parser
      @generator = code_generator
      @output = output
    end

    # Performs the compilation.
    def compile
      section = :code
      errors = []
      while true
        begin
          statement = @parser.parse_top_level

          break if statement == nil
          next if statement.empty?

          case statement[0]
          when :section
            section = statement[1]
          else
            @generator.add section, statement
          end

        rescue CodeGenerator::Error => e
          errors << e

        rescue Parser::MultipleErrors => e
          errors.concat e.errors

        rescue Parser::ParseError => e
          errors << e
        end

        if errors.length >= 100
          # Too many errors, give up.
          raise Error.new(errors)
        end
      end

      undefined_symbols = @generator.undefined_symbols
      unless undefined_symbols.empty?
        msg = "The following symbols are used, but have not been defined" +
          " or imported:\n" + undefined_symbols.to_a.sort!.join(" ")
        errors << RuntimeError.new(msg)
      end

      if errors.empty?
        @generator.write @output
      else
        raise Error.new(errors)
      end
    end
  end
end
