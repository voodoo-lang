require 'voodoo/validator'

module Voodoo
  # Voodoo parser.
  # The parser reads Voodoo[http://inglorion.net/documents/designs/voodoo/]
  # source code and turns it into Ruby[http://www.ruby-lang.org/] objects.
  #
  # The public interface to Parser consists of the methods #new and
  # #parse_top_level
  #
  # Example usage:
  #
  #   require 'voodoo/parser'
  #
  #   File.open('example.voo') do |infile|
  #     parser = Voodoo::Parser.new infile
  #
  #     while (element = parser.parse_top_level)
  #       puts element.inspect
  #     end
  #   end
  class Parser
    NUMBER_STARTER = /\d|-/
    STRING_STARTER = '"'
    SYMBOL_STARTER = /[[:alpha:]]|_|\\/
    
    # Creates a parser using the specified object as input.
    # The input object must support a method +getc+, which must
    # return the next character of the input, or +nil+ to indicate
    # the end of the input has been reached.
    def initialize input
      @input = input
      @input_name = input.respond_to?(:path) ? input.path : nil
      @start_line = @line = 1
      @start_column = @column = 0
      @lookahead = nil
      @text = ''
    end

    # Base class for errors reported from the parser.
    # This provides methods to get the name of the input being processed,
    # as well as the start_line, start_column, and text of the code
    # that triggered the error.
    class Error < Voodoo::Error
      def initialize message, input_name, start_line, start_column, text
        super message
        @input_name = input_name
        @start_line = start_line
        @start_column = start_column
        @text = text
      end

      attr_reader :input_name, :start_line, :start_column, :text
    end

    # Class for parse errors.
    # A ParseError indicates an error in the code being parsed.
    # For other errors that the parser may raise, see ParserInternalError.
    class ParseError < Parser::Error
      def initialize message, input_name, start_line, start_column, text
        super message, input_name, start_line, start_column, text
      end
    end

    # Class for parser internal errors.
    # A ParserInternalError indicates an error in the parser that is not
    # flagged as an error in the code being parsed. Possible causes
    # include I/O errors while reading code, as well as bugs in the
    # parser.
    # 
    # The +cause+ attribute indicates the initial cause for the error.
    # The other attributes of ParserInternalError are inherited from
    # Parser::Error and indicate the input that was being
    # processed when the error occurred.
    class ParserInternalError < Parser::Error
      def initialize cause, input_name, start_line, start_column, text
        super cause.message, input_name, start_line, start_column, text
        @cause = cause
      end

      attr_reader :cause
    end

    # Class wrapping multiple Parser::Errors.
    class MultipleErrors < Parser::Error
      def initialize errors
        @errors = errors
        super(nil, errors[0].input_name, errors[0].start_line,
              errors[0].start_column, nil)
      end

      attr_reader :errors

      def message
        if @message == nil
          msg = "Multiple errors:\n\n"
          @errors.each do |error|
            msg << error.input_name << ":" if error.input_name
            msg << "#{error.start_line}: " << error.message
            if error.text != nil
              msg << "\n\n  #{error.text.gsub("\n", "\n  ")}"
            end
            msg << "\n"
          end
          @message = msg
        end
        @message
      end

      def text
        if @text == nil
          texts = @errors.map {|error| error.text}
          @text = texts.join "\n"
        end
        @text
      end
    end

    # Parses a top-level element.
    # Returns an array containing the parts of the element.
    #
    # Some examples (Voodoo code, Ruby return values in comments):
    #
    #   section functions
    #   # [:section, :functions]
    #   
    #   call foo x 12
    #   # [:call, :foo, :x, 12]
    #   
    #   set x add x 42
    #   # [:set, :x, :add, :x, 42]
    #   
    #   set-byte @x 1 10
    #   # [:"set-byte", [:"@", :x], 1, 10]
    #   
    #   ifeq x y
    #       set z equal
    #   else
    #       set z not-equal
    #   end if
    #   # [:ifeq, [:x, :y], [[:set, :z, :equal]], [[:set, :z, :"not-equal"]]]
    #   
    #   foo:
    #   # [:label, :foo]
    #   
    #   function x y
    #       let z add x y
    #       return z
    #   end function
    #   # [:function, [:x, :y], [:let, :z, :add, :x, :y], [:return, :z]]
    #
    def parse_top_level
      wrap_exceptions do
        @text = ''
        # Skip whitespace, comments, and empty lines
        skip_to_next_top_level

        validate_top_level do
          parse_top_level_nonvalidating
        end
      end
    end

    # Parses statements up to "end X". _kind_ should indicate the type
    # of body being parsed: :block, :conditional, :function, or :group.
    # Returns an array of statements.
    def parse_body kind
      wrap_exceptions do
        body = []
        errors = []
        case kind
        when :function
          kind_text = 'function definition'
        else
          kind_text = kind.to_s
        end
        # Groups are allowed to contain top-level statements.
        # All other kinds aren't.
        top_level = kind == :group
        done = false
        until done
          begin
            with_position do
              statement = parse_top_level_nonvalidating
              if statement == nil
                done = true
                parse_error "End of input while inside #{kind_text}", nil

              elsif statement[0] == :end
                # Done parsing body
                done = true
              elsif kind == :conditional && statement[0] == :else
                # Done parsing body, but there is another one coming up
                body << statement
                done = true
              else
                # Should be a normal statement. Validate it, then add it to body
                begin
                  if top_level
                    Validator.validate_top_level statement
                  else
                    Validator.validate_statement statement
                  end
                  body << statement
                rescue Validator::ValidationError => e
                  magic_word = statement[0]
                  if !top_level &&
                      Validator::TOP_LEVELS.member?(magic_word) &&
                      !Validator::STATEMENTS.member?(magic_word)
                    parse_error "#{magic_word} is only allowed at top-level"
                  else
                    parse_error e.message
                  end
                end
              end
            end
          rescue => e
            # Got some kind of error. Still try to parse the rest of the body.
            errors << e
          end
        end

        # Raise error if we had just one.
        # If we had more than one, raise a MultipleErrors instance.
        if errors.length == 1
          raise errors[0]
        elsif errors.length > 1
          raise MultipleErrors.new errors
        end

        body
      end
    end

    # Parses an escape sequence.
    # This method should be called while the lookahead is the escape
    # character (backslash). It decodes the escape sequence and returns
    # the result as a string.
    def parse_escape
      wrap_exceptions do
        result = nil
        consume
        case lookahead
        when :eof
          parse_error "Unexpected end of input in escape sequence", nil
        when "\\", "\"", " "
          result = lookahead
          consume
        when "n"
          # \n is newline
          consume
          result = "\n"
        when "r"
          # \r is carriage return
          consume
          result = "\r"
        when "x"
          # \xXX is byte with hex value XX
          code = @input.read 2
          @column = @column + 2
          consume
          @text << code
          result = [code].pack('H2')
        when "\n"
          # \<newline> is line continuation character
          consume
          # Skip indentation of next line
          while lookahead =~ /\s/
            consume
          end
          result = ''
        else
          # Default to just passing on next character
          result = lookahead
          consume
        end
        result
      end
    end

    # Parses a number.
    # This method should be called while the lookahead is the first
    # character of the number.
    def parse_number
      wrap_exceptions do
        text = lookahead
        consume
        while lookahead =~ /\d/
          text << lookahead
          consume
        end
        text.to_i
      end
    end

    # Parses a string.
    # This method should be called while the lookahead is the opening
    # double quote.
    def parse_string
      wrap_exceptions do
        consume
        result = ''
        while true
          case lookahead
          when "\""
            consume
            break
          when "\\"
            result << parse_escape
          else
            result << lookahead
            consume
          end
        end
        result
      end
    end

    # Parses a symbol.
    # This method should be called while the lookahead is the first
    # character of the symbol name.
    def parse_symbol
      parse_symbol1 ''
    end

    # Continues parsing a symbol.
    # +name+ the part of the symbol that has already been parsed.
    def parse_symbol1 name
      wrap_exceptions do
        while lookahead != :eof
          case lookahead
          when "\\"
            name << parse_escape
          when /\w|-/
            name << lookahead
            consume
          when ':'
            # Colon parsed as last character of the symbol name
            name << lookahead
            consume
            break
          else
            break
          end
        end
        name.to_sym
      end
    end

    #
    # Private methods
    #
    private

    # Consumes the current lookahead character.
    # The character is appended to @text.
    def consume
      old = @lookahead
      @lookahead = nil
      if old == "\n"
        @line = @line.succ
        @column = 0
      end
      @text << old
      old
    end

    # Tests if a symbol is a label
    def is_label? symbol
      symbol.to_s[-1] == ?:
    end

    # Tests if a symbol is a conditional starter.
    def is_conditional? symbol
      [:ifeq, :ifge, :ifgt, :ifle, :iflt, :ifne].member? symbol
    end

    # Returns the current lookahead character,
    # or +:eof+ when the end of the input has been reached.
    def lookahead
      if @lookahead == nil
        @lookahead = @input.getc
        if @lookahead == nil
          @lookahead = :eof
        else
          @lookahead = @lookahead.chr
          @column = @column.succ
        end
      end
      @lookahead
    end

    # Parses a conditional statement.
    def parse_conditional1 condition, operands
      # Parse first clause and condition for next clause
      consequent, next_condition = split_if_clause parse_body(:conditional)
      if next_condition == nil
        # No next clause
        alternative = []
      elsif next_condition == :else
        # Next clause is else without if
        alternative = parse_body :conditional
      else
        # Next clause is else with if
        alternative = [parse_conditional1(next_condition[0],
                                          next_condition[1])]
      end
      [condition, operands, consequent, alternative]
    end

    # Raises a ParseError at the current input position.
    def parse_error message, text = @text
      # Create the error object
      error = ParseError.new(message, @input_name, @start_line,
                             @start_column, text)

      # Set a backtrace to the calling method
      error.set_backtrace caller

      # If we are not at a new line, skip until the next line
      while @column > 1 && lookahead != :eof
        consume
      end

      # Raise the error
      raise error
    end

    # Parses a top-level incantation without validating it.
    def parse_top_level_nonvalidating
      # Skip whitespace, comments, and empty lines
      skip_to_next_top_level

      words = []
      while true
        # Parse next token
        skip_whitespace
        word = try_parse_token
        if word == nil
          # Word is nil; that means we did not get a token
          case lookahead
          when :eof
            # End of input
            break
          when "\n"
            # Newline
            consume
            # Exit the loop, but only if the line wasn't empty
            break unless words.empty?
          when "#"
            # Skip comment
            while lookahead != :eof && lookahead != "\n"
              consume
            end
          else
            parse_error "Unexpected character (#{lookahead}) in input"
          end
        else
          # Word is not nil - we got a token
          if words.empty? && word.kind_of?(::Symbol) && word.to_s[-1] == ?:
            # First word is a label
            words = [:label, word.to_s[0..-2].to_sym]
            break
          end
          # Add word to statement
          words << word
        end
        words
      end

      # We have a line of input. Conditionals and function declarations
      # must be handled specially, because they consist of more than one
      # line.
      if words.empty?
        # Nothing to parse; return nil
        nil
      elsif words[0] == :function
        # Function declaration. Parse function body
        body = parse_body :function
        [:function, words[1..-1]] + body
      elsif is_conditional?(words[0])
        parse_conditional1 words[0], words[1..-1]
      elsif words[0] == :block || words[0] == :group
        body = parse_body words[0]
        [words[0]] + body
      else
        # Statement or data declaration; simply return it
        words
      end
    end

    # Skips whitespace, newlines, and comments before a top-level incantation.
    def skip_to_next_top_level
      while true
        case lookahead
        when /\s/
          # Skip whitespace
          consume
        when "\n"
          # Newline
          consume
        when "#"
          # Skip comment
          while lookahead != :eof && lookahead != "\n"
            consume
          end
        else
          break
        end
      end
    end

    # Consumes characters until a character other than space or tab is
    # encountered.
    def skip_whitespace
      while lookahead == " " || lookahead == "\t"
        consume
      end
    end

    # Splits a parsed if-clause into two parts:
    # 1. The list of statements making up the clause proper
    # 2. The condition for the next clause:
    #    - If there is no next clause, nil
    #    - If the next clause is introduced by else without a condition, :else
    #    - If the next clause is introduced by else iflt x y, [:iflt [:x, :y]]
    #    - And so on for other if.. instances
    def split_if_clause clause
      last = clause[-1]
      if last.respond_to?(:[]) && last.length > 0 && last[0] == :else
        clause = clause[0..-2]
        if last.length > 1
          # Else if
          [clause, [last[1], last[2..-1]]]
        else
          # Only else
          [clause, :else]
        end
      else
        # No else
        [clause, nil]
      end
    end

    # Tries to parse a symbol, number, string, or at-expression. If
    # such a token starts at the current position, it is parsed and returned.
    # Else, nil is returned.
    def try_parse_token
      case lookahead
      when :eof
        nil
      when NUMBER_STARTER
        # Digit; parse number
        parse_number
      when "\\"
        # Check if this is the line continuation escape or some other escape.
        decoded = parse_escape
        if decoded == ''
          # Line continuation. Now that we've parsed that, try again.
          try_parse_token
        else
          # Some other escape. That means it's a symbol.
          parse_symbol1 decoded
        end
      when SYMBOL_STARTER
        # Letter, underscore, or backslash; parse symbol
        # Note: SYMBOL_STARTER matches digits and backslashes, too, so
        # keep it after the cases that match those.
       parse_symbol
      when STRING_STARTER
        # Double quote; parse string
        parse_string
      when '@'
        # Parse at-expression.
        # '@' must be followed by a number or symbol.
        consume
        case lookahead
        when NUMBER_STARTER
          expr = parse_number
        when SYMBOL_STARTER
          expr = parse_symbol
        else
          parse_error "Invalid character (#{lookahead}) " +
            "in at-expression; expecting number or symbol"
        end
        [:'@', expr]
      when '%'
	consume
	# Must be followed by a symbol.
	if lookahead !~ SYMBOL_STARTER
          parse_error "'%' must be followed by a symbol"
	end
	[:'%', parse_symbol]
      else
        # No valid starter for a token, return nil
        nil
      end
    end

    # Evaluates _block_ and checks that the result is a valid top-level
    # incantation.
    def validate_top_level &block
      with_position do
        result = yield
        begin
          if result != nil
            Validator.validate_top_level result
          end
          result
        rescue Validator::ValidationError => e
          parse_error e.message
        end
      end
    end

    # Evaluates block, keeping track of @start_line, @start_column
    # at the beginning of the block, and @text during the evaluation
    # of block.
    def with_position &block
      # Save old values
      old_line = @start_line
      old_column = @start_column
      old_text = @text

      # Evaluate block with new values
      begin
        @start_line = @line
        @start_column = @column
        @text = ''
        wrap_exceptions do
          yield
        end
      ensure
        # Restore old values
        @start_line = old_line
        @start_column = old_column
        @text = old_text + @text
      end
    end

    # Ensures that any exceptions that escape from block are instances of
    # Parser::Error.
    def wrap_exceptions &block
      begin
        yield
      rescue Parser::Error
        # Already an instance of Parser::Error; pass it through.
        raise
      rescue => e
        # Some other error; wrap in ParserInternalError.
        wrapped = ParserInternalError.new(e, @input_name, @line,
                                          @column, @text)
        wrapped.set_backtrace e.backtrace
        raise wrapped
      end
    end
    
  end
end
