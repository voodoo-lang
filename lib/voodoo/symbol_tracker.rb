require 'set'

module Voodoo
  # Class to keep track of defined and used symbols.
  class SymbolTracker
    def initialize
      @defined = Set.new
      @used = Set.new
    end

    def define *symbols
      @defined.merge symbols
    end

    def use *symbols
      @used.merge symbols
    end

    def defined_but_unused_symbols
      @defined - @used
    end

    def defined_symbols
      @defined
    end

    def used_symbols
      @used
    end

    def used_but_undefined_symbols
      @used - @defined
    end
  end
end
