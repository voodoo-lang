require 'delegate'
require 'tempfile'
require 'voodoo/generators/arm_gas_generator'
require 'voodoo/generators/gas_elf_generator'

module Voodoo
  # Generator that produces ELF objects for arm
  class ARMELFGenerator < DelegateClass(ARMGasGenerator)
    def initialize params = {}
      @asmgenerator = ARMGasGenerator.new params
      super(@asmgenerator)
      @elfgenerator = GasELFGenerator.new @asmgenerator
    end

    def output_file_name input_name
      @elfgenerator.output_file_name input_name
    end

    def output_file_suffix
      @elfgenerator.output_file_suffix
    end

    def write io
      @elfgenerator.write io
    end
  end

  # Register class for arm
  Voodoo::CodeGenerator.register_generator ARMELFGenerator,
                                           :architecture => :arm,
                                           :format => :elf

end
