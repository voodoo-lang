require 'delegate'
require 'tempfile'
require 'voodoo/generators/i386_nasm_generator'
require 'voodoo/generators/nasm_elf_generator'

module Voodoo
  # Generator that produces ELF objects for i386
  class I386ELFGenerator < DelegateClass(I386NasmGenerator)
    def initialize params = {}
      @nasmgenerator = I386NasmGenerator.new params
      super(@nasmgenerator)
      @elfgenerator = NasmELFGenerator.new @nasmgenerator, '-f elf32'
    end

    def output_file_name input_name
      @elfgenerator.output_file_name input_name
    end

    def output_file_suffix
      @elfgenerator.output_file_suffix
    end

    def write io
      @elfgenerator.write io
    end
  end

  # Register class
  Voodoo::CodeGenerator.register_generator I386ELFGenerator,
                                           :architecture => :i386,
                                           :format => :elf
end
