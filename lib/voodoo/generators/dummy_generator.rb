require 'set'
require 'voodoo/symbol_tracker'

module Voodoo
  # Generator that does not generate code.
  class DummyGenerator
    def initialize *params
      @locals = Set.new
      @symbol_tracker = SymbolTracker.new
    end

    def add section, *code
      analyze code
    end

    # Returns a set of symbols that have been used, but not defined.
    def undefined_symbols
      @symbol_tracker.used_but_undefined_symbols
    end

    def write *args
    end

    private

    def analyze statements
      statements.each { |code| analyze_statement code }
    end

    def analyze_statement code
      case code[0]
      when :block
        old_locals = @locals
        @locals = Set.new @locals
        begin
          analyze code[1..-1]
        ensure
          @locals = old_locals
        end
      when :export
        use *code[1..-1]
      when :function
        old_locals = @locals
        @locals = Set.new @locals
        @locals.merge code[1]
        begin
          analyze code[2..-1]
        ensure
          @locals = old_locals
        end
      when :import
        define *code[1..-1]
      when :label
        define code[1]
      when :let
        define code[1]
        analyze_expr code[2..-1]
      when :return
        analyze_expr code[1..-1]
      when :set
        analyze_values [code[1]]
        analyze_expr code[2..-1]
      else
        if code[0].to_s[0...2] == "if"
          analyze_values code[1]
          analyze code[2]
          analyze code[3] if code.length > 3
        else
          analyze_values code[1..-1]
        end
      end
    end

    def analyze_expr code
      if code.length == 1
        analyze_values code
      else
        analyze_values code[1..-1]
      end
    end

    def analyze_values values
      values.each do |x|
        if x.kind_of? Symbol
          use x
        elsif x.respond_to?(:[]) && x[0] == :'@'
          analyze_values x[1]
        end
      end
    end

    def define *symbols
      @symbol_tracker.define *symbols
    end

    def use *symbols
      syms = Set.new symbols
      nonlocals = syms - @locals
      @symbol_tracker.use *nonlocals
    end
  end
end
