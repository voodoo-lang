require 'delegate'
require 'tempfile'
require 'voodoo/generators/mips_gas_generator'
require 'voodoo/generators/gas_elf_generator'

module Voodoo
  # Generator that produces ELF objects for mips and mipsel
  class MIPSELFGenerator < DelegateClass(MIPSGasGenerator)
    def initialize params = {}
      @asmgenerator = MIPSGasGenerator.new params
      super(@asmgenerator)
      case params[:architecture]
      when :mips
        byte_order = ' -EB'
      when :mipsel
        byte_order = ' -EL'
      else
        byte_order = ''
      end
      opts = '-KPIC' + byte_order
      @elfgenerator = GasELFGenerator.new @asmgenerator, opts
    end

    def output_file_name input_name
      @elfgenerator.output_file_name input_name
    end

    def output_file_suffix
      @elfgenerator.output_file_suffix
    end

    def write io
      @elfgenerator.write io
    end
  end

  # Register class for mips
  Voodoo::CodeGenerator.register_generator MIPSELFGenerator,
                                           :architecture => :mips,
                                           :format => :elf

  # Register class for mipsel
  Voodoo::CodeGenerator.register_generator MIPSELFGenerator,
                                           :architecture => :mipsel,
                                           :format => :elf
end
