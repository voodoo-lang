require 'delegate'
require 'tempfile'
require 'voodoo/generators/amd64_nasm_generator'
require 'voodoo/generators/nasm_elf_generator'

module Voodoo
  # Generator that produces ELF objects for amd64
  class AMD64ELFGenerator < DelegateClass(AMD64NasmGenerator)
    def initialize params = {}
      @nasmgenerator = AMD64NasmGenerator.new params
      super(@nasmgenerator)
      @elfgenerator = NasmELFGenerator.new @nasmgenerator, '-f elf64'
    end

    def output_file_name input_name
      @elfgenerator.output_file_name input_name
    end

    def output_file_suffix
      @elfgenerator.output_file_suffix
    end

    def write io
      @elfgenerator.write io
    end
  end

  # Register class
  Voodoo::CodeGenerator.register_generator AMD64ELFGenerator,
                                           :architecture => :amd64,
                                           :format => :elf
end
