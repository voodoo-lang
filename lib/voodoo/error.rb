module Voodoo
  # Base class of errors raised by the Voodoo Compiler.
  class Error < StandardError
  end
end
