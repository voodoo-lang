#### Common functionality for running tests.

# Make sure that the lib directory is in $LOAD_PATH
$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'thread'

class AtomicInteger
  def initialize initial_value = 0
    @value = 0
    @mutex = Mutex.new
  end

  def increment x = 1
    @mutex.synchronize { @value = @value + x }
  end

  def value
    @mutex.synchronize { @value }
  end

  def value= x
    @mutex.synchronize { @value = x }
  end

  def to_i
    value
  end

  def to_s
    value.to_s
  end
end

$failed = AtomicInteger.new
$passed = AtomicInteger.new
$tests = Queue.new

# Fails a test.
# +reason+ Should be a short message describing the reason for failure.
# +stderr+ If not empty, this text is written to standard error.
def fail_test reason = $!, stderr = ''
  $failed.increment
  puts "FAIL: #{reason}"
  $stdout.flush
  unless stderr.empty?
    $stderr.puts stderr
    $stderr.flush
  end
  false
end

# Passes a test.
def pass_test
  $passed.increment
  puts "pass"
  $stdout.flush
  true
end

# Runs block and verifies that it returns the given value.
# If it doesn't, prints an error message and increments $failed.
def expect name, value, &block
  begin
    print "#{name}..."
    result = yield
    if result == value
      pass_test
    else
      fail_test "expected #{value.inspect}, but got #{result.inspect}"
    end
  rescue Exception => e
    fail_test "Unexpected #{e.class}\n#{e.message}"
  end
end

# Runs block and verifies that it returns true.
# If it doesn't, prints an error message and increments $failed.
def expect_true name, &block
  expect name, true, &block
end

# Runs block and verifies that it throws an exception of type
# +exception_type+. If the block doesn't throw such an exception,
# prints an error message and increments $failed.
def expect_exception name, exception_type, &block
  begin
    print "#{name}..."
    yield
    fail_test "Expected exception of type #{exception_type}," +
      " but no exception was raised"
  rescue Exception => e
    if e.kind_of? exception_type
      pass_test
    else
      fail_test "Expected #{exception_type}, but got #{e.class}"
    end
  end
end

$RUBY = ENV['RUBY'] || 'ruby'
$VOODOOC = 'env RUBYLIB=../lib ../bin/voodooc'

# Runs a command with exec.
# With no block given, returns
# [status, stdin, stdout, stderr]
# With block given, passes
# status, stdin, stdout, stderr to block
def popen4 *command
  pw = IO::pipe   # pipe[0] for read, pipe[1] for write
  pr = IO::pipe
  pe = IO::pipe
  
  pid = fork do
    # child
    pw[1].close
    STDIN.reopen(pw[0])
    pw[0].close
    
    pr[0].close
    STDOUT.reopen(pr[1])
    pr[1].close
    
    pe[0].close
    STDERR.reopen(pe[1])
    pe[1].close
    
    exec *command
  end

  # parent
  pw[0].close
  pr[1].close
  pe[1].close
  dummy, status = Process.wait2 pid
  result = [status, pw[1], pr[0], pe[0]]
  pw[1].sync = true
  if block_given?
    begin
      return yield(*result)
    ensure
      [pw[1], pr[0], pe[0]].each { |p| p.close unless p.closed? }
    end
  end
  result
end

def add_test program, command, expected_output = '', params = {}
  $tests << [program, command, expected_output, params]
end

def run_test program, command, expected_output = '', params = {}
  input = params.has_key?(:input) ? params[:input] : nil
  expected_status = params.has_key?(:expected_status) ?
    params[:expected_status] : 0
  expected_errors = params.has_key?(:expected_errors) ?
    params[:expected_errors] : ''
  expected_signal = params.has_key?(:expected_signal) ?
    params[:expected_signal] : nil

  message = "#{program}..."
  status, stdin, stdout, stderr = popen4 command
  if input
    stdin.write input
  end
  exitstatus = status.exited? ? status.exitstatus : (status.to_i >> 8)
  signal = status.signaled? ? status.termsig : nil
  output = stdout.read
  err_output = stderr.read
  if expected_signal && signal != expected_signal
    message << "FAIL: expected signal #{expected_signal} but got "
    if signal
      message << "signal #{signal}"
    else
      message << "no signal"
    end
    $failed.increment
  elsif !expected_signal && signal
    message << "FAIL: got signal #{signal} while not expecting a signal"
    $failed.increment
  elsif exitstatus != expected_status
    message << "FAIL: exit status is #{exitstatus}, expected #{expected_status}"
    $failed.increment
  elsif output != expected_output
    message << "FAIL: wrong output"
    $stderr.puts "--- OUTPUT ---\n#{output}\n--- EXPECTED ---\n#{expected_output}\n--- END OUTPUT ---"
    $failed.increment
  elsif err_output != expected_errors
    message << "FAIL: wrong error output"
    $stderr.puts "--- ERRORS ---\n#{err_output}\n--- EXPECTED ---\n#{expected_errors}\n--- END ERRORS ---"
    $failed.increment
  else
    message << 'pass'
    $passed.increment
  end
  puts message
end

def add_test2 program, expected_output
  add_test program, "./#{program}", expected_output
end

def add_test1 program
  add_test2 program, `cat #{program}.out`
end

# Reports tests results.
# Returns 0 if all tests passed, 1 if some failed.
def report_test_results
  puts "#{$passed.value} tests passed, #{$failed.value} tests failed"
  if $failed.value == 0
    exit 0
  else
    exit 1
  end
end

# Runs all tests in the given queue.
# Each test is described by a list
# [name, command, expected_output, params].
# Those parameters are passed to run_test.
def run_tests queue, nthreads = 1
  threads = []
  nthreads.times do
    threads << Thread.new do
      until queue.empty?
        name, command, output, params = queue.pop
        run_test name, command, output, params
      end
    end
  end
  threads.each { |t| t.join }
end
