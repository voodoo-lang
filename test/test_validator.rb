#! /usr/bin/env ruby

require File.dirname(__FILE__) + '/test'
require 'voodoo/validator'

include Voodoo::Validator

# Runs block and verifies that it throws a ValidationError.
# If the block doesn't throw such an exception,
# prints an error message and increments $errors.
def expect_ValidationError name, &block
  expect_exception name, ValidationError, &block
end

def test_validator
  [:add, :and, :asr, :bsr, :div, :mod, :mul,
   :or, :rol, :ror, :shl, :shr, :sub, :xor].each do |binop|
    expect_true("#{binop}_ints") {
      validate_expression [binop, -14, 3]
    }

    expect_ValidationError("#{binop}_missing_arg") {
      validate_expression [binop, 3]
    }

    expect_ValidationError("#{binop}_no_args") {
      validate_expression [binop]
    }

    expect_ValidationError("#{binop}_too_many_args") {
      validate_expression [binop, -14, 3, 28]
    }
  end

  expect_true("align_no_parameters") {
    validate_top_level [:align]
  }

  expect_ValidationError("align_no_integer") {
    validate_top_level [:align, "wrong"]
  }

  expect_true("align_with_parameter") {
    validate_top_level [:align, 16]
  }

  expect_ValidationError("align_too_many_parameters") {
    validate_top_level [:align, 16, "wrong"]
  }

  expect_ValidationError("auto-bytes_missing_parameter") {
    validate_expression [:'auto-bytes']
  }

  expect_true("auto-bytes_expression") {
    validate_expression [:'auto-bytes', 23456]
  }

  expect_ValidationError("auto-bytes_too_many_parameters") {
    validate_expression [:'auto-bytes', 23456, :oops]
  }

  expect_true("at_number_expression") {
    validate_expression [:'@', 40]
  }

  expect_ValidationError("at_string_expression") {
    validate_expression [:'@', "foo"]
  }

  expect_true("at_symbol_expression") {
    validate_expression [:'@', :foo]
  }

  expect_ValidationError("block_expression") {
    validate_expression [:block, [:call, :foo]]
  }

  expect_true("block_statement") {
    validate_statement [:block, [:call, :foo]]
  }

  expect_true("block_level") {
    validate_top_level [:block, [:call, :foo]]
  }

  expect_true("byte_top_level") {
    validate_top_level [:byte, 42]
  }

  expect_ValidationError("byte_top_level_no_integer") {
    validate_top_level [:byte, "wrong"]
  }

  [:call, :"tail-call"].each do |call|
    expect_true("#{call}_expression") {
      validate_expression [call, :foo]
    }

    expect_true("#{call}_expression_multiple_parameters") {
      validate_expression [call, :foo, :bar, 42]
    }

    expect_true("#{call}_statement") {
      validate_statement [call, :foo]
    }

    expect_true("#{call}_top_level") {
      validate_top_level [call, :foo]
    }

    expect_ValidationError("#{call}_without_parameters") {
      validate_expression [call]
    }
  end

  expect_ValidationError("function_as_expression") {
    validate_expression [:function, [:x, :y], [:return, [:add, :x, :y]]]
  }

  expect_ValidationError("function_as_statement") {
    validate_statement [:function, [:x, :y], [:return, [:add, :x, :y]]]
  }

  expect_ValidationError("function_in_function") {
    validate_statement [:function, [],
                        [:function, [], [:return, 0]], [:return, 0]]
  }

  expect_ValidationError("function_missing_formals") {
    validate_top_level [:function]
  }

  expect_true("function_ok") {
    validate_top_level [:function, [:x, :y], [:return, [:add, :x, :y]]]
  }

  [:byte, :word].each do |thing|
    expect_true("get-#{thing}_expression") {
      validate_expression [:"get-#{thing}", :foo, 12]
    }

    expect_ValidationError("get-#{thing}_expression_missing_parameter") {
      validate_expression [:"get-#{thing}", :foo]
    }

    expect_ValidationError("get-#{thing}_expression_no_parameters") {
      validate_expression [:"get-#{thing}"]
    }

    expect_ValidationError("get-#{thing}_expression_too_many_parameters") {
      validate_expression [:"get-#{thing}", :foo, 12, 13]
    }

    expect_ValidationError("get-#{thing}_statement") {
      validate_statement [:"get-#{thing}", :foo, 12]
    }

    expect_ValidationError("set-#{thing}_expression") {
      validate_expression [:"set-#{thing}", :foo, 12, 18]
    }

    expect_true("set-#{thing}_statement") {
      validate_statement [:"set-#{thing}", :foo, 12, 18]
    }

    expect_ValidationError("set-#{thing}_statement_missing_parameters") {
      validate_statement [:"set-#{thing}", :foo]
    }

    expect_ValidationError("set-#{thing}_statement_no_parameters") {
      validate_statement [:"set-#{thing}"]
    }

    expect_ValidationError("set-#{thing}_statement_too_many_parameters") {
      validate_statement [:"set-#{thing}", :foo, 12, 18, 19]
    }
  end

  expect_ValidationError("goto_expression") {
    validate_expression [:goto, 8888]
  }

  expect_true("goto_statement_int") {
    validate_statement [:goto, 8888]
  }

  expect_true("goto_statement_label") {
    validate_statement [:goto, :foo]
  }

  expect_ValidationError("goto_statement_no_parameters") {
    validate_statement [:goto]
  }

  expect_ValidationError("goto_statement_too_many_parameters") {
    validate_statement [:goto, :foo, 42]
  }

  expect_true("goto_top_level") {
    validate_top_level [:goto, :foo]
  }

  expect_true("group") {
    validate_top_level [:group, [:word, 42], [:string, "xyzzy\x00"]]
  }

  expect_true("group_empty") {
    validate_top_level [:group]
  }

  expect_ValidationError("group_invalid_statement") {
    validate_top_level [:group, [:invalid]]
  }

  [:ifeq, :ifge, :ifgt, :ifle, :iflt, :ifne].each do |cnd|
    expect_ValidationError("#{cnd}_expression") {
      validate_expression [cnd, [:x, :y], [[:call, :foo]]]
    }

    expect_true("#{cnd}_else_statement") {
      validate_statement [cnd, [:x, :y], [[:call, :foo]], [[:call, :bar]]]
    }

    expect_true("#{cnd}_statement") {
      validate_statement [cnd, [:x, :y], [[:call, :foo]]]
    }

    expect_true("#{cnd}_top_level") {
      validate_top_level [cnd, [:x, :y], [[:call, :foo]]]
    }

    expect_ValidationError("let_inside_#{cnd}_statement") {
      validate_statement [cnd, [:x, :y], [[:let, :foo, 42]]]
    }

    expect_true("let_inside_block_inside_#{cnd}_statement") {
      validate_statement [cnd, [:x, :y], [[:block, [:let, :foo, 42]]]]
    }
  end

  [:export, :import].each do |directive|
    expect_true("#{directive}_top_level") {
      validate_top_level [directive, :foo]
    }

    expect_true("#{directive}_top_level_multiple_parameters") {
      validate_top_level [directive, :foo, :bar, :baz]
    }

    expect_ValidationError("#{directive}_top_level_no_parameters") {
      validate_top_level [directive]
    }

    expect_ValidationError("#{directive}_statement") {
      validate_statement [directive, :foo]
    }
  end

  expect_true("label_statement") {
    validate_statement [:label, :foo]
  }

  expect_ValidationError("label_statement_no_parameters") {
    validate_statement [:label]
  }

  expect_ValidationError("label_statement_too_many_parameters") {
    validate_statement [:label, :foo, 18]
  }

  expect_ValidationError("label_statement_parameter_no_symbol") {
    validate_statement [:label, 18]
  }

  expect_true("label_top_level") {
    validate_top_level [:label, :foo]
  }

  expect_true("let_inside_block") {
    validate_top_level [:block, [:let, :foo, 42], [:call, :foo]]
  }

  expect_true("let_inside_function") {
    validate_top_level [:function, [:n],
                       [:let, :foo, [:mul, :n, :n]],
                       [:call, :foo]]
  }

  expect_true("let_statement_int") {
    validate_statement [:let, :x, 12]
  }

  expect_true("let_statement_expression") {
    validate_statement [:let, :x, [:add, 12, 3]]
  }

  expect_ValidationError("let_statement_no_symbol") {
    validate_statement [:let, 12, 12]
  }

  expect_ValidationError("let_statement_without_parameters") {
    validate_statement [:let]
  }

  expect_true("int_is_expression") {
    validate_expression 12
  }

  expect_ValidationError("no_symbol_expression") {
    validate_expression ["wrong"]
  }

  expect_ValidationError("no_symbol_statement") {
    validate_statement ["wrong"]
  }

  expect_ValidationError("no_symbol_top_level") {
    validate_top_level ["wrong"]
  }

  expect_ValidationError("no_array_statement") {
    validate_statement :wrong
  }

  expect_ValidationError("no_array_top_level") {
    validate_top_level :wrong
  }

  expect_true("not_expression") {
    validate_expression [:not, :x]
  }

  expect_ValidationError("not_expression_no_parameters") {
    validate_expression [:not]
  }

  expect_ValidationError("not_expression_too_many_parameters") {
    validate_expression [:not, :x, :y]
  }

  expect_ValidationError("section_missing_name") {
    validate_top_level [:section]
  }

  expect_ValidationError("section_number") {
    validate_top_level [:section, 12]
  }

  expect_true("section_string") { validate_top_level [:section, :code] }

  expect_true("section_symbol") { validate_top_level [:section, :code] }

  expect_true("set_statement_int") {
    validate_statement [:set, :x, 12]
  }

  expect_ValidationError("set_statement_no_symbol") {
    validate_statement [:set, 12, 12]
  }

  expect_true("set_statement_expression") {
    validate_statement [:set, :x, [:add, 12, 3]]
  }

  expect_ValidationError("set_statement_without_parameters") {
    validate_statement [:set]
  }

  expect_true("string_top_level") {
    validate_top_level [:string, "test"]
  }

  expect_ValidationError("string_top_level_no_string") {
    validate_top_level [:string, 42]
  }

  expect_true("symbol_is_expression") {
    validate_expression :foo
  }

  expect_true("word_top_level") {
    validate_top_level [:word, 42]
  }

  expect_ValidationError("word_top_level_no_integer") {
    validate_top_level [:word, "wrong"]
  }
end

if $0 == __FILE__
  test_validator
  exit report_test_results
end
