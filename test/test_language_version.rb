#! /usr/bin/env ruby

#### Tests that all code generators support the expected version
#### of the Voodoo language.

VOODOO_VERSION = "1.1"

require File.dirname(__FILE__) + '/test'
require 'voodoo'

def test_language_version
  Voodoo::CodeGenerator::architectures.each do |arch|
    Voodoo::CodeGenerator::formats(arch).each do |format|
      generator = Voodoo::CodeGenerator.get_generator(:architecture => arch,
                                                      :format => format)

      expect_true "#{generator.class} supports voodoo" do
        generator.has_feature? :voodoo
      end

      expect_true "#{generator.class} supports voodoo #{VOODOO_VERSION}" do
        version = generator.features[:voodoo]
        version == VOODOO_VERSION
      end
    end
  end
end

if $0 == __FILE__
  test_language_version
  exit report_test_results
end
