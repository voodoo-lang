# Test that output_file_name and output_file_suffix produce the
# right results for all code generators

# List of test cases.
# For each test case, lists:
# 1. The architecture
# 2. The format
# 3. The expected suffix
tests = [[:amd64, :elf, '.o'],
         [:amd64, :nasm, '.asm'],
         [:i386, :elf, '.o'],
         [:i386, :nasm, '.asm'],
         [:mips, :elf, '.o'],
         [:mips, :gas, '.s'],
         [:mipsel, :elf, '.o'],
         [:mipsel, :gas, '.s']]

require 'voodoo'

prefix = 'plover'

errors = 0

tests.each do |test|
  arch, fmt, suffix = test
  generator = Voodoo::CodeGenerator.get_generator :architecture => arch,
                                                  :format => fmt
  
  x = generator.output_file_suffix
  if x != suffix
    $stderr.puts "#{arch.inspect}, #{fmt.inspect} -- " +
      "expected #{suffix.inspect}, but got #{x.inspect}"
    errors = errors + 1
  end

  expect = prefix + suffix
  y = generator.output_file_name "#{prefix}.voo"
  if y != expect
    $stderr.puts "#{arch.inspect}, #{fmt.inspect} -- " +
      "expected #{expect.inspect}, but got #{y.inspect}"
    errors = errors + 1
  end
  
end

if errors == 0
  puts "pass"
else
  puts "#{errors} tests failed"
  exit 1
end
