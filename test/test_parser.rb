#! /usr/bin/env ruby

require File.dirname(__FILE__) + '/test'
require 'voodoo/parser'

# Class that provides a getc method that reads from a string.
class StringReader
  def initialize str
    @str = str
    @pos = 0
  end

  def getc
    if @pos < @str.length
      x = @str[@pos]
      @pos = @pos + 1
      x
    else
      nil
    end
  end
end

# Like StringReader, but will throw an error after n characters.
class BrokenStringReader < StringReader
  def initialize str, n
    super str
    @n = n
  end

  def getc
    if @pos == @n
      raise StandardError, "Error generated by BrokenStringReader"
    end
    super
  end
end

# Runs a test on the parser.
#
# +name+      Name of the test
# +input+     Input to the test
#
# If given a block, returns the result of yielding to the block.
# The block receives two arguments:
# +result+    Result from the parser (nil if an error was raised)
# +error+     Error that was raised (nil if no error was raised)
def parser_test_ex name, input
  print "#{name}..."
  result = []
  error = nil

  begin
    if input.respond_to? :getc
      reader = input
    else
      reader = StringReader.new input
    end
    parser = Voodoo::Parser.new reader
    while (x = parser.parse_top_level) != nil
      result << x
    end
  rescue => e
    result = nil
    error = e
  end

  yield result, error
end

# Runs a test on the parser.
#
# +name+      Name of the test
# +input+     Input to the test
# +expected+  Expected return value from the parser
# +exception+ Expected exception class, or nil if no exception expected
def parser_test name, input, expected, exception = nil
  parser_test_ex(name, input) do |result, error|
    if exception == nil
      # Not expecting exception.
      if error == nil
        # Got no exception.
        if result == expected
          pass_test
        else
          fail_test("result did not match expected result",
                    "Got:\n#{result.inspect}\nExpected:\n#{expected.inspect}")
        end
      else
        # Wasnt't expecting exception, but got one.
        fail_test("unexpected exception of type #{error.class}",
                  "#{error.class}:#{error.message}\n" +
                  (error.kind_of?(Voodoo::Parser::Error) ?
                   "#{error.start_line}: #{error.text}\n" : '') +
                  error.backtrace.join("\n"))
      end
    else
      # Expecting exception.
      if error == nil
        # Got no exception.
        fail_test "expected exception of class #{exception.class}, " +
          "but got none"
      elsif error.class != exception.class
        # Got exception, but of wrong type.
        fail_test("expected exception of class #{exception.class}, " +
                  "but got one of type #{error.class}",
                  "#{error.class}:#{error.message}\n" +
                  (error.kind_of?(Voodoo::Parser::Error) ?
                   "#{error.start_line}: #{error.text}\n" : '') +
                  error.backtrace.join("\n"))
      else
        # Got exception of expected type.
        pass_test
      end
    end
  end
end

# Runs a test on the parser and verifies that the parser
# throws an exception of the expected type, with the expected
# line and column information.
#
# +name+        Name of the test
# +input+       Input to the test
# +error_type+  Expected error type
# +line+        Expected line number
# +column+      Expected column number
# +text+        Expected text from the source code
def parser_test_error name, input, error_type, line, column, text
  # Test I/O error on the first character.
  parser_test_ex(name, input) do |result, error|
    if error.class != error_type
      fail_test("expecting exception of class #{error_type}, " +
                "but got #{error.class}",
                "#{error.class}:#{error.message}\n" +
                error.backtrace.join("\n"))
    elsif error.start_line != line || error.start_column != column
      fail_test("wrong position. Expected line #{line}, column #{column}" +
                "; got line #{error.start_line}, column #{error.start_column}")
    elsif error.text != text
      fail_test("wrong text.",
                "===== ACTUAL =====\n#{error.text}\n" +
                "===== EXPECTED =====\n#{text}\n")
      $stderr.puts error.backtrace
    else
      pass_test
    end
  end
end

def test_parser
  parser_test "empty", "", []

  parser_test "newline", "\n", []

  parser_test "comment", "# only a comment", []

  parser_test "comment_newline", "# only a comment\n", []

  parser_test "multiline_comment", "# only a comment\n# multiline", []

  parser_test "multiline_comment_newline",
    "# only a comment\n# multiline\n", []

  parser_test "section", "section functions", [[:section, :functions]]

  parser_test "section_newline", "section functions\n",
    [[:section, :functions]]

  parser_test "section_comment", "section functions # functions section",
    [[:section, :functions]]

  parser_test "function_noargs", "function\nreturn 0\nend function\n",
    [[:function, [], [:return, 0]]]

  parser_test "function_onearg", "function x\nreturn x\nend function\n",
    [[:function, [:x], [:return, :x]]]

  parser_test "function_twoargs",
    "function x y\nreturn add x y\nend function\n",
    [[:function, [:x, :y], [:return, :add, :x, :y]]]

  parser_test "continued", "call foo \\\n    bar", [[:call, :foo, :bar]]

  parser_test "continued_number", "call foo \\\n    42", [[:call, :foo, 42]]

  parser_test_error("io_error_start",
                    BrokenStringReader.new("section data", 0),
                    Voodoo::Parser::ParserInternalError,
                    1, 0,
                    '')

  parser_test_error("io_error_first_char",
                    BrokenStringReader.new("section data", 1),
                    Voodoo::Parser::ParserInternalError,
                    1, 1,
                    's')

  parser_test_error("io_error_second_line",
                    BrokenStringReader.new("section data\nfoo:", 13),
                    Voodoo::Parser::ParserInternalError,
                    2, 0,
                    '')

  parser_test_ex("multiple_errors", "block\nlet\nset\n") do |result,error|
    expected_message = <<EOT
Multiple errors:

2: let requires a symbol and an expression

  let
  
3: set requires a symbol or at-expression followed by an expression

  set
  
4: End of input while inside block
EOT
    expected_text = <<EOT
let

set

EOT

    if !error.kind_of? Voodoo::Parser::MultipleErrors
      fail_test "Expected MultipleErrors, but got #{error.class}"
    elsif error.errors.length != 3
      fail_test "Expected 3 errors, but got #{error.errors.length}"
    elsif error.errors[0].start_line != 2
      fail_test "Expected first error on line 2" +
        ", but got it on line #{error.errors[0].start_line}"
    elsif error.errors[1].start_line != 3
      fail_test "Expected second error on line 3" +
        ", but got it on line #{error.errors[1].start_line}"
    elsif error.errors[2].start_line != 4
      fail_test "Expected third error on line 4" +
        ", but got it on line #{error.errors[2].start_line}"
    elsif error.message != expected_message
      fail_test("Error message differs from expected",
                "===== ACTUAL =====\n#{error.message}\n" +
                "===== EXPECTED =====\n#{expected_message}\n")
    elsif error.text != expected_text
      fail_test("Error text differs from expected",
                "===== ACTUAL =====\n#{error.text}\n" +
                "===== EXPECTED =====\n#{expected_text}\n")
    else
      pass_test
    end
  end
end

if $0 == __FILE__
  test_parser
  exit report_test_results
end
