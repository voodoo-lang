<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
<title>The Voodoo Programming Language</title>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
<div class="body">
<h1>The Voodoo Programming Language</h1>


<p class="first">This document describes version 1.1 of the Voodoo
programming language. Definitions in this document have been marked
with the version of the language in which they have been
introduced. Definitions first introduced by this document have been
marked with <strong class="since current">1.1</strong>.</p>

<h2>Hello World in Voodoo</h2>

<p class="first">To quickly get a feeling for a programming language,
it is often instructive to look at a simple yet complete program.
The following is an implementation of the traditional
Hello World program in Voodoo:</p>

<pre><code>
# Hello world in Voodoo

section data
greeting:
string "Hello, world!\x00"

section functions
import puts
export main

main:
function argc argv
    call puts greeting
    return 0
end function
</code></pre>

<p class="first">When compiled and linked with a library that provides
an appropriate implementation of the <code>puts</code> function, this
program will print <q>Hello, world!</q>.</p>

<p>What follows is a more formal description of the Voodoo
programming language.</p>

<h2>Overview</h2>

<p class="first">The source code of a Voodoo program consists of
<em>incantations</em> (e.g. <code>set x 42</code>) that define the code
and data of the program.  Each incantation starts with a <em>magic
word</em> (e.g. <code>set</code>) and may have zero or
more <em>parameters</em>. Any incantation may be preceded by
a <em>label</em> (e.g. <code>foo:</code>), which can then be used in
other parts of the program to refer to the <em>address</em> at which the
incantation starts.</p>

<p>The basic units of data in Voodoo are <em>bytes</em>
and <em>words</em>. The range and size of bytes and words is
<em>implementation-defined</em>, which means that the definition
is allowed to differ from one Voodoo implementation to another,
and even in ways defined by a specific implementation, e.g. the
same Voodoo implementation may use different word sizes for
different target platforms.</p>

<p>Most incantations in Voodoo perform very simple operations, such as
adding two numbers. In addition to these simple operations, Voodoo
supports <em>blocks</em> and <em>functions</em>. A block provides a
scope for <em>local variables</em> and automatically managed memory.
Variables and memory allocated inside a block will automatically be
de-allocated after the end of that block. Functions, like blocks,
provide a scope for local variables and automatically managed memory.
In addition, functions may accept <em>parameters</em>, which are local
variables whose values are supplied by the <em>caller</em> of the
function.</p>

<p>At run-time, entering a function or block creates a
<em>frame</em> and makes it the new <em>active frame</em>. These
frames form a chain which logically runs from the <em>top-level
frame</em> at the top to the active frame at the bottom. The active
frame determines what local variables are <em>accessible</em>.
The magic word <code>goto</code> can be used to continue execution
of the program at a given incantation, provided that incantation
is in the scope corresponding to the active frame.</p>

<p>This document does not define the correct behavior of every
possible Voodoo program. In places where the correct behavior is not
defined (either through omission, or explicitly documented as being
undefined), a Voodoo implementation is not required to implement any
specific behavior. In particular, the behavior of a program that
invokes undefined behavior at any point does not have to be repeatable
or desirable. Later versions of the Voodoo language may define correct
behavior for programs whose behavior is undefined under the current
language version.</p>

<h2>Features</h2>
<p class="first"><strong class="since current">1.1</strong> To aid authors and programs in
generating code that is compatible with a particular Voodoo
implementation, implementations should provide means to query their
features. This document specifies a number of features as key-value
pairs, where the key is the feature name, and the value is a string
that provides additional information about the feature (as described
in the specification of the feature).</p>

<p>Implementations may also advertise features not specified in this
document. To prevent conflicts with future versions of Voodoo, these
features should have names starting in a prefix that identifies the
implementation that introduced the feature. Implementations are
encouraged to use this mechanism to advertise support for extensions
to the language specified by this document.</p>

<p>For language extensions advertised using the feature mechanism, it
is recommended that the value of the feature be a version number, with
higher versions being backward compatible with lower versions.
Incompatible changes can be indicated by a different feature name,
e.g. through a numeric suffix at the end of the feature name.</p>

<table summary="Features">
<thead>
<tr><th>Name</th><th>Description</th></tr>
</thead>
<tbody>
<tr>
<td>bits-per-word</td>
<td>The value indicates the number of bits per word for the given
implementation. E.g. <q>32</q>.</td>
</tr>
<tr>
<td>byte-order</td>
<td>Order of bytes in a word. The following values are defined:
<dl>
<dt>big-endian</dt>
<dd>Bytes are ordered from most significant to least significant</dd>
<dt>little-endian</dt>
<dd>Bytes are ordered from least significant to most significant</dd>
</dl></td>
</tr>
<tr>
<td>bytes-per-word</td>
<td>The number of bytes required to store a word. E.g. <q>4</q>.</td>
</tr>
<tr>
<td>voodoo</td>
<td>Version of the Voodoo language supported by the implementation.
The version described in this document is <q>1.1</q>.</td>
</tr>
</tbody>
</table>
<h2>Tokens</h2>
<h3>Comments</h3>

<p class="first"><strong class="since">1.0</strong> Comments start with a hash mark (#)
and run until the end of the line. The following is an example
comment:</p>

<pre><code>
# This is an example comment
</code></pre>

<h3>Integers</h3>

<p class="first"><strong class="since">1.0</strong> Integers consist of an optional
plus or minus sign, followed by one or more digits. Examples of valid
integers:</p>

<pre><code>
0
+12
-1
</code></pre>

<h3>Strings</h3>

<p class="first"><strong class="since">1.0</strong> Strings consist of zero or more
bytes enclosed in double quotes. An escape mechanism is provided to
allow bytes of any value to be included in strings (see
<a href="#escape_sequences">Escape Sequences</a> for details).
Examples of valid strings:</p>

<pre><code>
""
"Hello, world!"
"some \"escape\x00 sequences"
</code></pre>

<h3>Symbols</h3>

<p class="first"><strong class="since">1.0</strong> Symbols consist of letters, digits,
underscores, hyphens, and escape sequences. Only a letter, underscore,
or escape sequence may be used for the first byte of a
symbol. Although this definition allows any byte to be part of a
symbol, not all symbol names may be compatible with all target
platforms. This document does not specify how implementations
should represent symbols, and thus does not require that all
syntactically valid symbols be usable on every implementation on
every target platform. Examples of valid symbols:</p>

<pre><code>
foo
some_symbol
</code></pre>

<h3>Substitute Tokens</h3>

<p class="first"><strong class="since current">1.1</strong> To aid in the writing of programs
that are portable to multiple target platforms and Voodoo implementations,
Voodoo defines a number of tokens that implementations must substitute
appropriate values for.</p>

<p>A symbol prefixed by a percent character (%) must be treated as
equivalent to an integer value. The following table shows the percent
substitutes that must be recognized by Voodoo implementations:</p>

<table summary="Percent substitutes"><tr>
<th>Symbol</th>
<th>Meaning</th>
</tr><tr>
<td><code>%saved-frame-size</code></td>
<td>The number of bytes required to save a frame</td>
</tr><tr>
<td>any feature that maps to an integer</td>
<td>the integer value of the feature</td>
</tr></table>

<p class="first">The effect of using a substitute token whose meaning
is not defined above is undefined.</p>

<p>Examples of valid substitute tokens:</p>

<table summary="Substitute token examples"><tr>
<td><code>%bytes-per-word</code></td>
<td>number of bytes per word</td>
</tr></table>

<h2>Escape Sequences</h2>

<p class="first"><strong class="since">1.0</strong> To facilitate entering bytes that
may be treated differently by different systems and to inhibit the
special meaning certain bytes normally have, an escape mechanism is
provided. For example, the escape mechanism can be used to create
strings that contain double quotes, or to spread a single incantation
over multiple lines. Escape sequences start with a backslash, and have
the following meanings:</p>

<table summary="Special bytes"><tr>
<td><code>\\</code></td>
<td>An actual backslash</td>
</tr><tr>
<td><code>\"</code></td>
<td>A double quote</td>
</tr><tr>
<td><code>\n</code></td>
<td>A newline</td>
</tr><tr>
<td><code>\r</code></td>
<td>A carriage return</td>
</tr><tr>
<td><code>\t</code></td>
<td>A tab</td>
</tr><tr>
<td><code>\x<var>XX</var></code></td>
<td>The byte with hexadecimal value <var>XX</var></td>
</tr><tr>
<td><code>\&lt;space&gt;</code></td>
<td>A space</td>
</tr><tr>
<td><code>\&lt;newline&gt;</code></td>
<td>Line continuation. The newline
and any leading whitespace on the next line
is ignored.</td>
</tr></table>

<p class="first">Examples of the usage of escape sequences:</p>

<table summary="Example Escape Sequences"><tr>
<td><code>"Hello&nbsp;world\n"</code></td>
<td>A string with a newline in it</td>
</tr><tr>
<td><code>"\\\""</code></td>
<td>A string consisting of a backslash followed by a double quote</td>
</tr><tr>
<td><code>call foo \<br />
 bar</code></td>
<td>Same as: <code>call&nbsp;foo&nbsp;bar</code></td>
</tr></table>

<h2>Labels</h2>

<p class="first"><strong class="since">1.0</strong> Places in a program can be marked
with labels, so that the place can be referred to from the code by
mentioning the label. A label consists of a symbol followed by a
colon. When referring to a label, only the symbol is used, without the
colon.</p>

<p class="first">For example:</p>

<pre><code>
foo:
word 12
</code></pre>

<p class="first">declares a word with the value 12, and a label 'foo'
that refers to the place where this value is stored. For example, if
the value happens to be loaded at the address 71234, writing 'foo' in
the program will have the same effect as writing 71234 in the same
place.</p>

<h2>Values</h2>

<p class="first"><strong class="since">1.0</strong> Values are the smallest unit of
data in a Voodoo program. Examples of values are:</p>

<table summary="Example Values"><tr>
<td><code>12</code></td>
<td>The integer 12</td>
</tr><tr>
<td><code>x</code></td>
<td>The value of x (may be a label or a local variable)</td>
</tr></table>

<h2>At-Expressions</h2>

<p class="first"><strong class="since">1.0</strong> The character @ can be used to
denote the value at some address. An address prefixed with @ denotes
the word stored at that address, rather than the address itself. The
address may be an integer, a local variable, or a label. For
example:</p>

<table summary="Example at-expressions"><tr>
<td><code>@12</code></td>
<td>The word stored at address 12</td>
</tr><tr>
<td><code>@x</code></td>
<td>The word stored at address x</td>
</tr></table>

<p class="first">Syntactically, at-expressions are values. Therefore,
in any place where a value can be used, an at-expression can also be
used.</p>

<h2>Sections</h2>

<p class="first"><strong class="since">1.0</strong> A Voodoo program is divided in a number of sections.
In source code, these are introduced by an incantation of the form
<code>section &lt;identifier&gt;</code>, where &lt;identifier&gt; is an
identifier for the section.</p>

<p>The following section identifiers are valid:</p>

<table summary="Section Identifiers"><tr>
<th>Identifier</th>
<th>Meaning</th>
</tr><tr>
<td><code>code</code></td>
<td>This section contains executable code</td>
</tr><tr>
<td><code>data</code></td>
<td>This section contains data</td>
</tr><tr>
<td><code>functions</code></td>
<td>This section contains function definitions</td>
</tr></table>

<p class="first">Example usage of the <code>section</code> incantation:</p>

<pre><code>
section data
# define data here ...

section functions
# define functions here ...
</code></pre>

<p class="first">Sections may appear in the source code in any order.
The same section name may be used multiple times, in which case the
contents of the section will be the concatenation of the contents of
the multiple places in which it is defined.</p>

<h2>Data Definitions</h2>

<p class="first"><strong class="since">1.0</strong> Data can be inserted into a Voodoo
program by means of the magic
words <code>byte</code>, <code>word</code>, and
<code>string</code>, followed by the value of the byte, word, or
string to be inserted. For example:</p>

<pre><code>
# A byte with value 42
byte 42

# A word with value -1
word -1

# The string "hello"
string "hello"
</code></pre>

<h2>Groups</h2>

<p class="first"><strong class="since current">1.1</strong> Groups are a mechanism to group
multiple data definitions into a single unit. For example, to define
a data structure consisting of a word and two bytes:</p>

<pre><code>
example:
group
    word 42
    byte 0
    byte 1
end group
</code></pre>

<h2>Import and Export</h2>

<p class="first"><strong class="since">1.0</strong> To allow Voodoo programs to refer to definitions in
other files, and to allow other files to refer to definitions in
Voodoo programs, the magic words <code>import</code>
and <code>export</code> are used.</p>

<p>Using <code>import</code>, definitions from elsewhere are made
available to a Voodoo program:</p>

<pre><code>
section data
import stderr

section functions
import fputs

# We can now refer to stderr and fputs
</code></pre>

<p>Data and functions defined in a Voodoo program can be made
available to other programs using <code>export</code>:</p>

<pre><code>
section data
export answer

answer:
word 42

section functions
export foo

foo:
function
  # some code here
end function
</code></pre>

<p><code>export</code> exports the first definition that follows it.
Groups can be used to group together data that must be exported as a
single unit.</p>

<p>If symbols are imported or exported, the <code>import</code>
or <code>export</code> must occur before the first use of the symbol
in any section. To ensure portability, implementations should reject
programs that do not obey this constraint.</p>

<h2>Alignment</h2>

<p class="first"><strong class="since">1.0</strong> Many architectures require that data and/or code
observe certain alignment restrictions. For example, an architecture may
require that a word of data be at an address that is a multiple of the
word size. Voodoo provides the magic word <code>align</code>, which
specifies the alignment for the next program element.</p>

<p>Without any parameters, <code>align</code> inserts filler bytes
into the current section, so that the next element added to the section
will respect the default alignment for the section. The default
alignment for each section is implementation-defined, but must ensure
that the alignment restrictions of the target platform are observed.</p>

<p>When written as <code>align &lt;n&gt;</code>, where &lt;n&gt; is an
integer, the incantation will insert filler bytes as necessary to align
the next element to be added to the section on a multiple of &lt;n&gt;
bytes.</p>

<p>The filler bytes inserted by <code>align</code> are unspecified.
In particular, they are not guaranteed to be valid code.</p>

<p>Example uses of the <code>align</code> incantation:</p>

<pre><code>
section data

x:
byte 1

# Ensure that y is aligned according to
# the target platform's alignment restrictions
align
y:
word 42



section functions

# Ensure that foo is aligned according to
# the target platform's alignment restrictions
align
foo:
function n
    # some code here
end function
</code></pre>

<h2>Actions</h2>

<p class="first">Voodoo code consists of actions. Actions consist of a
magic word, usually followed by a number of values. This section lists
all actions supported by Voodoo. In the list below, '&lt;x&gt;',
'&lt;y&gt;', and '&lt;z&gt;' denote values, '&lt;symbol&gt;' denotes a
symbol, and '&lt;expr&gt;' denotes an expression.  (expressions are
discussed further on). Ellipsis (&hellip;) is used to denote that more
items may follow, and square brackets ([ and ]) are used to denote
that an item is optional.</p>

<dl>

<dt><strong class="since">1.0</strong> <code>call &lt;x&gt; &lt;y&gt; &lt;z&gt;
&hellip;</code></dt>
<dd><p class="first">Calls the function &lt;x&gt; with the parameters &lt;y&gt;
&lt;z&gt; &hellip;. There may be zero or more parameters.</p></dd>

<dt><strong class="since">1.0</strong> <code>goto &lt;x&gt;</code></dt>
<dd><p class="first">Continues the program at location &lt;x&gt;,
rather than at the next action after the goto.</p>

<p>Any value can be used as the location to go to. However, the
consequences of using <code>goto</code> to jump to a label outside
the active frame are undefined. Since frames are introduced by
blocks and functions, this means that <code>goto</code> should
not jump to a label in a different block or function, unless the
frame of that block or function has been restored using
<code>restore-frame</code>.</p></dd>

<dt><strong class="since">1.0</strong> <code>let &lt;symbol&gt; &lt;expr&gt;</code></dt>
<dd><p class="first">Introduces a local variable &lt;symbol&gt;, and
initializes it to the result of evaluating &lt;expr&gt;.</p>

<p>This action is only allowed inside functions or blocks, that is,
between <code>function</code> and the corresponding
<code>end function</code>, or between <code>block</code> and the
corresponding <code>end block</code>.</p>

<p>The scope of a variable introduced by <code>let</code> includes
every statement after the <code>let</code> action and before the
<code>end function</code> or <code>end block</code> that ends the
function or block in which the variable is introduced.</p></dd>

<dt><strong class="since current">1.1</strong> <code>restore-frame &lt;x&gt;</code></dt>

<dd><p class="first">Restores a frame that was saved at address
&lt;x&gt;. After this action, any frames below the restored frame
are no longer valid. The behavior of <code>restore-frame</code> is
undefined if &lt;x&gt; does not contain valid frame information.
The values of local variables are undefined after
<code>restore-frame</code> is performed.</p></dd>

<dt><strong class="since current">1.1</strong> <code>restore-locals &lt;x&gt;
    [&lt;locals&gt;]</code></dt>

<dd><p class="first">Sets local variables to values that have been
stored at address &lt;x&gt;. If one or more local variables are
specified, restores the specified local variables. Else, restores
all local variables in scope. The behavior is undefined if the
information saved at &lt;x&gt; does not contain a value for any of
the variables to be restored.</p></dd>

<dt><strong class="since">1.0</strong> <code>return [&lt;expr&gt;]</code></dt>
<dd><p class="first">Returns from the current function.
If &lt;expr&gt; is specified, it is evaluated and the function returns
the result of the evaluation. Otherwise, the return value is
unspecified.</p></dd>

<dt><strong class="since current">1.1</strong> <code>save-frame &lt;x&gt;</code></dt>

<dd><p class="first">Saves information about the current frame to
a block of memory at address &lt;x&gt;. The block of memory must be
large enough to hold the required information. Allocating
<code>%saved-frame-size</code> bytes ensures a block of memory large
enough to hold a saved frame.</p></dd>

<dt><strong class="since current">1.1</strong> <code>save-frame-and-locals &lt;x&gt;
    [&lt;locals&gt;]</code></dt>

<dd><p class="first">Saves information about the current frame and
local variables to a block of memory at address &lt;x&gt;. If one or
more local variables are specified, at least the named local variables
are saved. If no local variables are specified, all local variables
are saved.</p>

<p>The block of memory starting at &lt;x&gt; must be large enough to
hold the required
information. Allocating <code>%saved-frame-size</code> bytes ensures a
block of memory large enough to hold a saved frame plus local
variables.</p></dd>

<dt><strong class="since current">1.1</strong> <code>save-locals &lt;x&gt; [&lt;locals&gt;]</code></dt>

<dd><p class="first">Saves local variables to a block of memory at
address &lt;x&gt;. If one or more local variables are specified, at least the
named local variables are saved. If no local variables are specified,
all local variables are saved.</p>

<p>The block of memory starting at &lt;x&gt; must be large enough to
hold the required
information. Allocating <code>%saved-frame-size</code> bytes ensures a
block of memory that is large enough.</p>

<p>This action does not invalidate frame information or local
variables previously stored at &lt;x&gt;, except for updating the
stored values for the local variables it saves to the values these
local variables currently have.</p>
</dd>

<dt><strong class="since">1.0</strong> <code>set &lt;symbol&gt; &lt;expr&gt;</code></dt>
<dd><p class="first">Evaluates &lt;expr&gt; and assigns the result to 
&lt;symbol&gt;. &lt;symbol&gt; may not be a label, because labels cannot be 
assigned to.</p></dd>

<dt><strong class="since current">1.1</strong> <code>set &lt;at-expr&gt; &lt;expr&gt;</code></dt>
<dd><p class="first">Evaluates &lt;expr&gt; and stores the result at
the address specified by the at-expression &lt;at-expr&gt;.</p></dd>

<dt><strong class="since">1.0</strong> <code>set-byte &lt;base&gt; &lt;offset&gt; &lt;x&gt;</code></dt>
<dd><p class="first">Sets the byte at &lt;base&gt; + &lt;offset&gt; to &lt;x&gt;.
&lt;offset&gt; is given as a number of bytes.</p></dd>

<dt><strong class="since">1.0</strong> <code>set-word &lt;base&gt; &lt;offset&gt; &lt;x&gt;</code></dt>
<dd><p class="first">Sets the word at &lt;base&gt; + WORDSIZE *
&lt;offset&gt; to &lt;x&gt;.</p>

<p>The address computed by &lt;base&gt; + WORDSIZE * &lt;offset&gt;
is expected to be a multiple of the word size.
The behavior of <code>set-word</code> is undefined if this condition
is not satisfied.
</p></dd>

<dt><strong class="since">1.0</strong> <code>tail-call &lt;x&gt; &lt;y&gt; &lt;z&gt; ...</code></dt>
<dd><p class="first">Performs a tail call to the function &lt;x&gt;
with parameters &lt;y&gt; &lt;z&gt; &hellip;. This has an effect
similar to 'return call &lt;x&gt; &lt;y&gt; &lt;z&gt; ...', but
re-uses the call frame of the current function. This means that if
&lt;x&gt; takes fewer or at most as many parameters as the current
function, the tail call requires no extra space.</p></dd>
</dl>

<h2>Expressions</h2>

<p class="first">Certain actions have the ability to evaluate
expressions. Expressions can be simple values, but expressions can
also perform computations on values. The following are valid
expressions:</p>

<dl>
<dt><strong class="since">1.0</strong> <code>add &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The result of adding &lt;y&gt; to &lt;x&gt;.</p>

<p>If the result of the addition cannot be represented in a single word,
the result of <code>add</code> is undefined.</p></dd>

<dt><strong class="since">1.0</strong> <code>and &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The bitwise and of &lt;x&gt; and &lt;y&gt;.</p></dd>

<dt><strong class="since">1.0</strong> <code>asr &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Performs an arithmetic right shift. The bits in
&lt;x&gt; are shifted &lt;y&gt; positions to the right. The sign of
&lt;x&gt; is preserved, so that the result has the same sign as
&lt;x&gt;. The result is undefined if &lt;y&gt; is negative.</p></dd>

<dt><strong class="since current">1.1</strong> <code>auto-bytes &lt;x&gt;</code></dt>
<dd><p class="first">Allocates &lt;x&gt; bytes of memory. The
allocated bytes will automatically be released after the current frame
ends. The result is the address of the allocated bytes.</p></dd>

<dt><strong class="since current">1.1</strong> <code>auto-words &lt;x&gt;</code></dt>
<dd><p class="first">Allocates &lt;x&gt; words of memory. The
allocated words will automatically be released after the current frame
ends. The result is the address of the allocated words.</p></dd>

<dt><strong class="since">1.0</strong> <code>bsr &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Performs a bitwise right shift. The bits in
&lt;x&gt; are shifted &lt;y&gt; positions to the right. &lt;y&gt;
zero-valued bits are shifted in from the left. The result does
not necessarily have the same sign as &lt;x&gt;. The result is
undefined if &lt;y&gt; is negative.</p></dd>

<dt><strong class="since">1.0</strong> <code>call &lt;x&gt; &lt;y&gt; &lt;z&gt; ...</code></dt>
<dd><p class="first">Similar to the action call, this calls the
function &lt;x&gt; with the parameters &lt;y&gt; &lt;z&gt; ... (there
may be zero or more parameters). The result of this expression is the
value returned from the function.</p></dd>

<dt><strong class="since">1.0</strong> <code>div &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The (integer) result of dividing &lt;x&gt; by
&lt;y&gt;.</p>

<p>If &lt;x&gt; &ge; 0 and &lt;y&gt; &gt; 0, the result is the largest
integer equal to or less than the algebraic quotient of &lt;x&gt;
and &lt;y&gt;.</p>

<p>If either &lt;x&gt; or &lt;y&gt; is negative, the result is
implementation-defined.</p>

<p>If &lt;y&gt; is zero, or if the quotient cannot be represented in a
single machine word, the result is undefined.</p></dd>

<dt><strong class="since">1.0</strong> <code>get-byte &lt;base&gt; &lt;offset&gt;</code></dt>
<dd><p class="first">The value of the byte at address &lt;base&gt; + &lt;offset&gt;.</p></dd>

<dt><strong class="since">1.0</strong> <code>get-word &lt;base&gt; &lt;offset&gt;</code></dt>
<dd><p class="first">The value of the word at address &lt;base&gt; +
(WORDSIZE * &lt;offset&gt;).</p>

<p>The address computed as &lt;base&gt; + (WORDSIZE * &lt;offset&gt;) is
expected to be a multiple of the word size. If this condition is not met,
the behavior of <code>get-word</code> is undefined.</p></dd>

<dt><strong class="since">1.0</strong> <code>mod &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">For &lt;x&gt; &ge; 0 and &lt;y&gt; &gt; 0, returns
&lt;x&gt; modulo &lt;y&gt;.</p>

<p>If either &lt;x&gt; or &lt;y&gt; is negative, the result is
implementation-defined.</p>

<p>If &lt;y&gt; is zero, the result is undefined.</p></dd>

<dt><strong class="since">1.0</strong> <code>mul &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The result of multiplying &lt;x&gt; by &lt;y&gt;.</p>

<p>If the algebraic result of &lt;x&gt; * &lt;y&gt; cannot be represented
in a single word, the result of <code>mul &lt;x&gt; &lt;y&gt;</code>
contains only the low-order bits of the full result.</p></dd>

<dt><strong class="since">1.0</strong> <code>not &lt;x&gt;</code></dt>
<dd><p class="first">The ones' complement of &lt;x&gt;; i.e. all the
bits in &lt;x&gt; inverted.</p></dd>

<dt><strong class="since">1.0</strong> <code>or &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The bitwise or of &lt;x&gt; and &lt;y&gt;.</p></dd>

<dt><strong class="since">1.0</strong> <code>rol &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Rotates the bits in &lt;x&gt; to the left by
&lt;y&gt; positions. Bits that are rotated off the left are inserted
on the right.
The result is undefined if &lt;y&gt; is negative.</p></dd>

<dt><strong class="since">1.0</strong> <code>ror &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Rotates the bits in &lt;x&gt; to the right by
&lt;y&gt; positions. Bits that are rotated off the right are inserted
on the left.
The result is undefined if &lt;y&gt; is negative.</p></dd>

<dt><strong class="since">1.0</strong> <code>shl &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Performs a left shift. The bits in
&lt;x&gt; are shifted &lt;y&gt; positions to the left.
The result is undefined if &lt;y&gt; is negative.</p></dd>

<dt><strong class="since">1.0</strong> <code>shr &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Performs a right shift. The bits in
&lt;x&gt; are shifted &lt;y&gt; positions to the right. It is
implementation-defined whether this operation preserves the sign
of &lt;x&gt; (for operations with specific sign-preservation
properties, use asr or bsr).
The result is undefined if &lt;y&gt; is negative.</p></dd>

<dt><strong class="since">1.0</strong> <code>sub &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The result of subtracting &lt;y&gt; from &lt;x&gt;.</p>

<p>If the result of the subtraction cannot be represented in a single
word, the result of <code>sub</code> is undefined.</p></dd>

<dt><strong class="since">1.0</strong> <code>xor &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">The bitwise exclusive or of &lt;x&gt; and
&lt;y&gt;.</p></dd>
</dl>

<h2>Conditionals</h2>

<p class="first"><strong class="since">1.0</strong> Conditionals in Voodoo take the following form:</p>

<pre><code>if&lt;test&gt;
    ... some code here ...
else if&lt;test&gt;
    ... other code here ...
... more "else if" parts ...
else
    ... some code ...
end if
</code></pre>

<p class="first">There can be any number of <q>else if</q> parts, and
the final <q>else</q> clause is optional. The tests that are provided
are the following:</p>

<dl>
<dt><code>ifeq &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Tests if &lt;x&gt; is equal to &lt;y&gt;.</p></dd>

<dt><code>ifge &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Tests if &lt;x&gt; is greater than or equal to
&lt;y&gt;.</p></dd>

<dt><code>ifgt &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Tests if &lt;x&gt; is strictly greater than
&lt;y&gt;.</p></dd>

<dt><code>ifle &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Tests if &lt;x&gt; is less than or equal to
&lt;y&gt;.</p></dd>

<dt><code>iflt &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Tests if &lt;x&gt; is strictly less than
&lt;y&gt;.</p></dd>

<dt><code>ifne &lt;x&gt; &lt;y&gt;</code></dt>
<dd><p class="first">Tests if &lt;x&gt; is different from &lt;y&gt;.</p></dd>
</dl>

<h2>Function Definitions</h2>

<p class="first"><strong class="since">1.0</strong> A function definition looks like:</p>

<pre><code>
function x y z
    &lt;code&gt;
end function
</code></pre>

<p class="first">Here, the function being defined takes 3 parameters,
which can be referred to as <code>x</code>, <code>y</code>, and
<code>z</code> from the code inside the function body. A function may
have zero or more parameters and is practically always preceded by a
label, so that the function can be referenced from code.</p>

<p>A function should only be entered using <code>call</code>,
<code>tail-call</code>, or entering a prior invocation of the function
using <code>restore-frame</code> and <code>goto</code>. When calling
a function, the number of supplied parameters should equal the number
of parameters the function was declared with.
A function should only be left through <code>return</code>,
<code>tail-call</code>, or a valid combination
of <code>restore-frame</code> and <code>goto</code>.
Failing to meet these requirements results in undefined behavior.</p>

<h2>Blocks</h2>

<p class="first"><strong class="since">1.0</strong> Blocks can be used to define a scope in which local
variables (introduced with <code>let</code>) can be referred to. A
block looks like:</p>

<pre><code>
block
    &lt;code&gt;
end block
</code></pre>

<p class="first">Inside a block, variables may be introduced using
<code>let</code>. Such a variable is in scope (can be referred to)
from the first statement after the <code>let</code> until the
<code>end block</code> that terminates the block.</p>

<p>Blocks can be placed anywhere an action can be placed: at
top-level, inside functions, inside blocks, and inside conditionals.</p>
</div>
</body>
</html>
